package fr.atomix.spigotmessaging;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class SpigotMessaging extends JavaPlugin{

	private static Plugin plugin;
	
	@Override
	public void onEnable()
	{
		plugin = this;
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeMessaging");
		this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeMessaging", new PluginListener());
	}
	
	/**
	 * Connect a player to a server
	 * @param player The player
	 * @param serverName The server name
	 */
	public static void connectPlayer(Player player, String serverName)
	{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF(serverName);
		player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
	}
	
	/**
	 * Send a player to a server with a message that would be received when the player get connected
	 * @param player The player you want to connect
	 * @param serverName The server
	 * @param subChannel Your subchannel name as String
	 * @param message The message as a list of String you want to send
	 */
	public static void connectPlayer(Player player, String serverName, String subChannel, List<String>message)
	{
		message.add("delayed");
		SpigotMessaging.sendMessage(player, serverName, subChannel, message);
		SpigotMessaging.connectPlayer(player, serverName);
	}
	
	/**
	 * Send a plugin message to an other server
	 * @param player The player you wish to send the message (if you don't care : Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null); )
	 * @param serverName The server name as String
	 * @param subChannel Your subchannel name as String
	 * @param message You message as List of String
	 */
	public static void sendMessage(Player player, String serverName, String subChannel, List<String> message)
	{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(serverName);
		out.writeUTF(player.getName());
		out.writeUTF(subChannel);
		out.writeInt(message.size());
		for(String s : message)
		{
			out.writeUTF(s);
		}
		player.sendPluginMessage(plugin, "BungeeMessaging", out.toByteArray());
	}

	/**
	 * Lock down a server
	 * @param player You must provide a player to send a plugin message
	 * @param serverName The server you want to lock
	 */
	public static void lockServer(Player player, String serverName)
	{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(serverName);
		out.writeUTF(player.getName());
		out.writeUTF("lock");
		player.sendPluginMessage(plugin, "BungeeMessaging", out.toByteArray());
	}
	
	/**
	 * Unlock a server
	 * @param player
	 * @param serverName
	 */
	public static void unlockServer(Player player, String serverName)
	{
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(serverName);
		out.writeUTF(player.getName());
		out.writeUTF("unlock");
		player.sendPluginMessage(plugin, "BungeeMessaging", out.toByteArray());
	}
}
