package fr.atomix.bungeemessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

/**
 * 
 * @author Atomix
 *
 */
public class BungeeMessaging extends Plugin implements Listener{

	private static ProxyServer proxy;
	public static HashMap<String, ByteArrayDataOutput> playersToSend;
	public static HashMap<String, ServerInfo> serverToSend;
	public static List<ServerInfo> serverLocked;

	@Override
	public void onEnable()
	{
		proxy = this.getProxy();
		playersToSend = new HashMap<String, ByteArrayDataOutput>();
		serverToSend = new HashMap<String, ServerInfo>();
		serverLocked = new ArrayList<ServerInfo>();
		proxy.registerChannel("BungeeMessaging");
		proxy.getPluginManager().registerListener(this, this);
		proxy.getPluginManager().registerListener(this, new EventListener());
	}

	@EventHandler
	public void onPluginMessage(PluginMessageEvent e)
	{
		String channel = e.getTag();
		if(!channel.equals("BungeeMessaging"))
			return;
		ByteArrayDataInput in = ByteStreams.newDataInput(e.getData());
		String serverName = in.readUTF();
		String playerName = in.readUTF();
		String subChannel = in.readUTF();
		ServerInfo server = proxy.getServerInfo(serverName);
		switch(subChannel)
		{
		case "lock":
			if(!serverLocked.contains(server))
				serverLocked.add(server);
			break;
		case "unlock":
			if(serverLocked.contains(server))
				serverLocked.remove(server);
			break;
		default:
			int nbMessage = in.readInt();
			ByteArrayDataOutput out = ByteStreams.newDataOutput();
			out.writeUTF(subChannel);
			out.writeInt(nbMessage);
			List<String> message = new ArrayList<String>();

			ByteArrayDataInput copy = ByteStreams.newDataInput(e.getData());
			copy.readUTF();//Read server name
			copy.readUTF();//Read player name
			copy.readUTF();// Read subchannel
			copy.readInt();//Read nbMessage
			
			for(int i = 0; i < nbMessage; i++)
			{
				message.add(copy.readUTF());
			}
			for(String s : message)
			{
				out.writeUTF(s);
			}
			if(server == null)
				return;
			if(message.contains("delayed"))
			{
				playersToSend.put(playerName, out);
				serverToSend.put(playerName, server);
			}else
			{
				server.sendData("BungeeMessaging", out.toByteArray());
			}
			break;
		}
	}
}
