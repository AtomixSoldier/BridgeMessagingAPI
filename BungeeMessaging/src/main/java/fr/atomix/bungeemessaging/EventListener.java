package fr.atomix.bungeemessaging;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * 
 * @author Atomix
 *
 */
public class EventListener implements Listener{

	@EventHandler
	public void onPlayerConnect(ServerConnectedEvent e)
	{
		ProxiedPlayer player = e.getPlayer();
		if(!BungeeMessaging.playersToSend.containsKey(player.getName()))
			return;
		ServerInfo server = BungeeMessaging.serverToSend.get(player.getName());
		server.sendData("BungeeMessaging", BungeeMessaging.playersToSend.get(player.getName()).toByteArray());
		BungeeMessaging.playersToSend.remove(player.getName());
		BungeeMessaging.serverToSend.remove(player.getName());
	}
	
	@EventHandler
	public void serverLocked(ServerConnectEvent e)
	{
		if(!BungeeMessaging.serverLocked.contains(e.getTarget()))
			return;
		e.setCancelled(true);
		TextComponent message = new TextComponent("This server is locked");
		message.setColor(ChatColor.RED);
		e.getPlayer().sendMessage(message);
	}
}
